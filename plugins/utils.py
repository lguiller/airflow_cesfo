from airflow.models import Variable
from requests import Session
from urllib.parse import urlencode

def request_cesfo(ti):
    CESFO_URL = "https://www.e-chargement.com/identif_badge.Asp"
    session = Session()

    data = {
        "badge_div": Variable.get("badge_div"),
        "badge_number": Variable.get("badge_number"),
        "badge_nom": Variable.get("badge_nom"),
    }
    headers = {
        "Content-type": "application/x-www-form-urlencoded"
    }
    res = session.get(CESFO_URL, headers=headers, data=urlencode(data), allow_redirects=True)
    res.encoding = res.apparent_encoding

    assert res.status_code == 200, f"Error: status code ({res.status_code})"
    ti.xcom_push(key="html", value=res.text)
