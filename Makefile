.DEFAULT_GOAL := init
.PHONY: init

UID			= $(shell id -u)

build_cesfo_server:
	@$(MAKE) -C server --no-print-directory

init_env:
	@echo "AIRFLOW_UID=$(UID)" > .env
	@read -p "Badge name (lastName firstName): " BADGE_NAME;echo BADGE_NAME=$$BADGE_NAME >> .env
	@read -p "Badge number: " BADGE_NUMBER;echo BADGE_NUMBER=$$BADGE_NUMBER >> .env

init: init_env build_cesfo_server
	@mkdir -p ./dags ./logs ./plugins ./config ./data
	@docker compose up -d

clean:
	@docker compose down --rmi all --volumes --remove-orphans

fclean: clean
	@$(RM) -rf config data logs .env
