# Cesfo Balance Management

![Cesfo web server screenshot](image/Screenshot.png)

## Prerequisites

The command lines below are based on a Debian system but this can be
easily adapted to other Linux systems

Docker and docker compose must be installed ([official installation documentation](https://docs.docker.com/engine/install/debian/)):

``` console
# Add Docker's official GPG key:
sudo apt-get update
sudo apt-get install ca-certificates curl gnupg
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg

# Add the repository to Apt sources:
echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update

sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```

Add your current user to the sudo and docker groups:

``` console
usermod -aG sudo your_user 
usermod -aG docker your_user 
```

Restart your session and login as your current user to continue with the
installation

## From sources

The sources for Cesfo Balance Management can be downloaded from the
[Gitlab
repo](https://git.ias.u-psud.fr/DevTeam/cesfo_balance_management).

You can either clone the public repository:

``` console
git clone https://git.ias.u-psud.fr/plato_pdc/sas_pipeline/sas-workflows.git
```

Once you have a copy of the source, you can install it with:

``` console
make
```

Then precise your name and you badge number:

``` console
Badge name (lastName firstName): Guillerot Lucas
Badge number: 18147
```

## Accessing to Airflow and local web server

Airflow web server is accessible on [localhost:8080](http://localhost:8080)

And you Cesfo history on [localhost:5555](http://localhost:5555)

## Author

:bust_in_silhouette: [**Lucas Guillerot**](https://git.ias.u-psud.fr/lguiller)
