from airflow import DAG
from datetime import datetime
from datetime import timedelta
from airflow.operators.dummy import DummyOperator
from airflow.operators.python import BranchPythonOperator
from airflow.operators.python import PythonOperator
from airflow.models import Variable
from utils import request_cesfo


def estimate_meal_price(previous_balance, current_balance, previous_incoming_credit):
    return (previous_balance - (current_balance - previous_incoming_credit))

def parse_response(ti):
    from bs4 import BeautifulSoup

    parsed_html = BeautifulSoup(ti.xcom_pull(key="html"), "html.parser")
    data = parsed_html.find_all("td", {"class": "bold"})

    incoming_credit = data[3].text.replace(",", ".")
    #incoming_credit = f"5 \N{euro sign}"

    ti.xcom_push(key="data", value={"incoming_credit": incoming_credit})

def update_file(ti):
    from pathlib import Path

    badge_number = Variable.get("badge_number")
    badge_nom = Variable.get("badge_nom")
    file_path = Path(
        "/opt/airflow/data",
        "history.txt",
    )

    incoming_credit = ti.xcom_pull(key="data")["incoming_credit"]

    with file_path.open("r") as f:
        lines = f.readlines()
        last_line = lines[-1]
        data = last_line.split(";")
        data[2] = incoming_credit
        del lines[-1]
        lines.append(";".join(data))
    with file_path.open("w") as f:
        f.write("".join(lines))

def is_incoming_credit(ti):
    incoming_credit = ti.xcom_pull(key="data")["incoming_credit"]

    return (
        "end_task" if not int(incoming_credit.split(" ")[0])
        else "update_file_task"
    )

default_args = {
    "owner": "sas-admin",
    "start_date": datetime(2023, 10, 26),
    "retries": 10,
    "retry_delay": timedelta(seconds=30),
}

with DAG(
    dag_id="check_for_incoming_credit",
    default_args=default_args,
    schedule="0 6 * * *",
    catchup=False,
) as dag:

    start_task = DummyOperator(task_id="start_task")

    request_cesfo_task = PythonOperator(
        task_id="request_cesfo_task",
        python_callable=request_cesfo,
    )

    parse_response_task = PythonOperator(
        task_id="parse_response_task",
        python_callable=parse_response,
    )

    is_incoming_credit_task = BranchPythonOperator(
        task_id="is_incoming_credit_task",
        python_callable=is_incoming_credit,
    )

    update_file_task = PythonOperator(
        task_id="update_file_task",
        python_callable=update_file,
    )

    end_task = DummyOperator(
        task_id="end_task",
        trigger_rule="none_failed_min_one_success",
    )

    start_task >> request_cesfo_task >> parse_response_task
    parse_response_task >> is_incoming_credit_task >> [update_file_task, end_task]
    update_file_task >> end_task
