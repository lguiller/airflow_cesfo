from airflow import DAG
from datetime import datetime
from datetime import timedelta
from airflow.operators.dummy import DummyOperator
from airflow.operators.python import PythonOperator
from airflow.models import Variable
from utils import request_cesfo


def estimate_meal_price(previous_balance, current_balance, previous_incoming_credit):
    return (previous_balance - (current_balance - previous_incoming_credit))

def parse_response(ti):
    from bs4 import BeautifulSoup

    parsed_html = BeautifulSoup(ti.xcom_pull(key="html"), "html.parser")
    data = parsed_html.find_all("td", {"class": "bold"})

    current_balance = data[1].text.replace(",", ".")
    current_date = data[2].text.split(" ")[0]
    previous_balance = current_balance
    incoming_credit = data[3].text.replace(",", ".")
    previous_incoming_credit = f"0.0 \N{euro sign}"

    ti.xcom_push(
        key="data",
        value={
            "current_balance": current_balance,
            "current_date": current_date,
            "previous_balance": previous_balance,
            "incoming_credit": incoming_credit,
            "previous_incoming_credit": previous_incoming_credit,
        },
    )

def update_file(ti):
    from pathlib import Path

    badge_number = Variable.get("badge_number")
    badge_nom = Variable.get("badge_nom")
    file_path = Path(
        "/opt/airflow/data",
        "history.txt",
    )

    current_balance = ti.xcom_pull(key="data")["current_balance"]
    current_date = ti.xcom_pull(key="data")["current_date"]
    previous_balance = ti.xcom_pull(key="data")["previous_balance"]
    incoming_credit = ti.xcom_pull(key="data")["incoming_credit"]
    previous_incoming_credit = ti.xcom_pull(key="data")["previous_incoming_credit"]

    if file_path.exists():
        with file_path.open("r") as f:
            last_history_line = f.readlines()[-1]
            previous_balance = last_history_line.split(";")[0]
            previous_incoming_credit = last_history_line.split(";")[2]
    else:
        with file_path.open("w") as f:
            f.write("Account Balance;Meal Price;Incoming Credit;Date\n")

    meal_price = estimate_meal_price(
        float(previous_balance.split(" ")[0]),
        float(current_balance.split(" ")[0]),
        float(previous_incoming_credit.split(" ")[0])
    )
    with file_path.open("a") as f:
        f.write(f"{current_balance};{meal_price:.2f} \N{euro sign};{incoming_credit};{current_date}\n")

default_args = {
    "owner": "sas-admin",
    "start_date": datetime(2023, 10, 26),
    "retries": 10,
    "retry_delay": timedelta(seconds=30),
}

with DAG(
    dag_id="update_balance_history",
    default_args=default_args,
    schedule="0 9 * * *",
    catchup=False,
) as dag:

    start_task = DummyOperator(task_id="start_task")

    request_cesfo_task = PythonOperator(
        task_id="request_cesfo_task",
        python_callable=request_cesfo,
    )

    parse_response_task = PythonOperator(
        task_id="parse_response_task",
        python_callable=parse_response,
    )

    update_file_task = PythonOperator(
        task_id="update_file_task",
        python_callable=update_file,
    )

    end_task = DummyOperator(task_id="end_task")

    start_task >> request_cesfo_task >> parse_response_task >> update_file_task >> end_task
